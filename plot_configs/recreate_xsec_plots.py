from pathlib import Path
from vdmtools.io import read_one_or_many
import numpy as np

def statistics(values, errors, context):
    if context.get("count", 0) < 8:
        if "count" not in context:
            context["count"] = 0
        context["count"] += 1

        return np.mean(values), np.std(values)

    context["count"] = 0
    weights = errors ** -2
    avg = np.average(values, weights=weights)
    err = np.sqrt(np.average((values - avg) ** 2, weights=weights))
    return avg, err


VDM_RESULTS_DIR = "output"

# Read the data
paths = Path(f"{VDM_RESULTS_DIR}/analysed_data/").glob("8999*")
names = ["vdM1", "bi1", "bi2", "vdM2", "vdM3", "vdM4", "vdM5", "vdM6"]
many_results = read_one_or_many(paths, names)

# Select which plugins to load
plugins = ["EvolutionPerDetector"]

# Configure the plot
config = {
    "strategy_name": "EvolutionPerDetector",
    "strategy_args": {
        "quantity": "xsec",
        "error": "xsecErr",
        "latex": r"$\sigma_{\mathrm{vis}}$",
        "work_status": "Private Work",
        "stats_function": statistics,
        "cov_status": "as_is",
        "fmt": "s",
        "file_suffix": "_xsec",
        "save_pickle": True,
    },
    "context": None,
    "data": many_results,
}

# Add the config to the plots
plots = [config]