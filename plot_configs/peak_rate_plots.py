from pathlib import Path
from vdmtools.io import read_one_or_many
import numpy as np


VDM_RESULTS_DIR = "output"

# Read the data
path = Path(f"{VDM_RESULTS_DIR}/analysed_data/8999_28Jun23_230143_28Jun23_232943")
single_result = read_one_or_many(path, "VdM1")

# Select which plugins to load
plugins = ["NormalPerDetector"]

# Configure the plot
config_X = {
    "strategy_name": "NormalPerDetector",
    "strategy_args": {
        "quantity": "peak_X",
        "error": "peakErr_X",
        "latex": r"$\mathrm{Peak}_X$",
        "work_status": "Private Work",
        "cov_status": "as_is",
        "fmt": "s",
        "avg_places": 4,
        "err_places": 5,
        "file_suffix": "_peakX",
        # "save_pickle": True,
    },
    "context": None,
    "data": single_result,
}
config_Y = {
    "strategy_name": "NormalPerDetector",
    "strategy_args": {
        "quantity": "peak_Y",
        "error": "peakErr_Y",
        "latex": r"$\mathrm{Peak}_Y$",
        "work_status": "Private Work",
        "cov_status": "as_is",
        "fmt": "s",
        "avg_places": 4,
        "err_places": 5,
        "file_suffix": "_peakY",
        # "save_pickle": True,
    },
    "context": None,
    "data": single_result,
}

# Add the config to the plots
plots = [config_X, config_Y]